# Getting Started Guide

Here you will find [Quick Start: Using Docker](#quick-start-using-docker), [Quick Start: Not Using Docker](#quick-start-not-using-docker) and a more detailed [Full Guide](#full-guide) for NPARS (NGS Post-Pipeline Accuracy and Reproducibility System).

## Quick Start: Using Docker

Start here if you would like to quickly install and run the tool with the included test/sample dataset.

### Prerequisites

1. [Docker Engine](https://docs.docker.com/engine/install/) and [Docker Compose](https://docs.docker.com/compose/install/). Follow instructions to give non-root users the ability to run the two aforementioned tools. If not, you must prefix all Docker commands with **sudo**.
2. [Git](https://git-scm.com/)

### Installation

1. Clone GitLab repo:

   ```bash
   git clone https://gitlab.com/erichpeterson/npars-analysis.git
   ```
   
2. Download the pre-built SQLite database by clicking the following link (via web browser): [download](https://drive.google.com/file/d/1CLHeaUB6wRqj7k6ssMXnkc1-kzuwEm6B/view?usp=sharing) 

   **OR**

   Use the following commands can be used on a Linux-based command line:

   ```bash
   fileId=1CLHeaUB6wRqj7k6ssMXnkc1-kzuwEm6B
   
   fileName=MockGMU_20.db
   
   curl -sc /tmp/cookie "https://drive.google.com/uc?export=download&id=${fileId}" > /dev/null
   
   code="$(awk '/_warning_/ {print $NF}' /tmp/cookie)"
   
   curl -Lb /tmp/cookie "https://drive.google.com/uc?export=download&confirm=${code}&id=${fileId}" -o ${fileName}
   ```

3. Change permissions on the directory and the downloaded database to allow all write permissions. 

   For example on Linux-based machines:

   ```bash
   chmod 766 /path/to/sqlite/directory/ && chmod 766 /path/to/sqlite/directory/*
   ```

4. Create a directory to store the outputs of the software and give full permissions to it

   For example on Linux-based machines:

   ```bash
   chmod 766 /path/to/output/directory/
   ```

### Running Software

#### Build & Run Docker Containers

2. Change directories to the directory in which the GitLab repo was cloned to.

2. Open the file **docker-compose.yml** and under each of the sections labeled **volumes**, edit the first half of the string (i.e., the half before the colon **:**) to the paths defined to have the database file and the output directory (as indicated in the comments of the file).

3. Run the Docker Compose command below to build the Docker images (will take ~ 30-40 minutes on a single core/high-memory machine).

   ```bash
   docker-compose build --force-rm --no-cache --parallel
   ```

4. Run the Docker containers using Docker Compose:

   ```bash
   docker-compose up
   ```

#### Execute R-based Analysis

1. Open a web browser from the machine running the Docker containers, and navigate to **http://127.0.0.1:48787**
2. When the login window appears, enter the following credentials: Username: **rstudio** and Password: **123**

##### Execute EDA Code

4. Click **File --> Open File** and type **/src** in the **File name** search box and hit **ENTER**. Click **Notebooks**. Choose **EDA_Analysis.Rmd** and **Open**.

5. Set your working directory by clicking **Session --> Set Working Directory --> To Source File Location**

6. Click the **Knit** button in the upper-left part of the screen (~ 5-10 minutes on a single-core/high-memory machine)

7. After completion of execution, an HTML file called **EDA_Analysis.html** will be placed in the output directory you previously specified.

##### Execute Differential Analysis Code

8. Click **File --> Open File** and type **/src** in the **File name** search box and hit **ENTER**. Click **Notebooks**. Choose **Differential_Analysis.Rmd** and **Open**.
9. Set your working directory by clicking **Session --> Set Working Directory --> To Source File Location**
10. Click the **Knit** button in the upper-left part of the screen (~ 15 minutes on a single-core/high-memory machine)
11. After completion of execution, an HTML file called **Differential_Analysis.html** will be placed in the output directory you specified and it can be viewed using your browser of choice. In addition, a directory called **reports** will be present which contains a series of HTML documents which contain the results from a differential analysis performed via DESeq2.

#### Execute Python-base Analysis

1. Copy the full URL that is displayed at the command line where you ran Docker. The URL will take the form **http://127.0.0.1:8888/?token=stringofcharacters**
2. Open a web browser of your choice, and navigate to the URL copied in the previous step.
3. A Python Jupyter Notbook file hierarchy should be displayed. Click **Notebooks --> Clustering.ipynb**
4. Click on **Cell --> Run All** This will run all Python-based analysis code blocks and display the results within the notebook.

#### Stop Docker Containers

1. At the command prompt where Docker was invoked, enter **CTRL-C** to stop the containers.

2. Enter the following command to remove the containers:

   ```bash
   docker-compose down
   ```

   

## Quick Start: Not Using Docker

### Prerequisites

1. [R](https://www.r-project.org/) v4.1.1 or greater
2. [RStudio](https://www.rstudio.com/) v1.4 or greater
3. [Python](https://www.python.org/) v3
4. [Git](https://git-scm.com/)

### Installation

1. Clone GitLab repo:

   ```bash
   git clone https://gitlab.com/erichpeterson/npars-analysis.git
   ```

2. Download the pre-built SQLite database by clicking the following link (via web browser): [download](https://drive.google.com/file/d/1CLHeaUB6wRqj7k6ssMXnkc1-kzuwEm6B/view?usp=sharing) 

   **OR**

   Use the following commands can be used on a Linux-based command line:

   ```bash
   fileId=1CLHeaUB6wRqj7k6ssMXnkc1-kzuwEm6B
   
   fileName=MockGMU_20.db
   
   curl -sc /tmp/cookie "https://drive.google.com/uc?export=download&id=${fileId}" > /dev/null
   
   code="$(awk '/_warning_/ {print $NF}' /tmp/cookie)"
   
   curl -Lb /tmp/cookie "https://drive.google.com/uc?export=download&confirm=${code}&id=${fileId}" -o ${fileName}
   ```

3. Change permissions on the directory and the downloaded database to allow all write permissions.

   For example on Linux-based machines:

   ```bash
   chmod 766 /path/to/sqlite/directory/ && chmod 766 /path/to/sqlite/directory/*
   ```

### Running Software

#### Executing R-based Analysis

##### Install R Libraries

1. Open R
2. Click **File --> Open File** and navigate the directory you cloned the Git repo into, **Open** the file **R/Packages.r**
3. Click the **Source** button to execute the commands, that will install all of the neccessary libraries.

##### Executing EDA Code

1. Click **File --> Open File** and navigate the directory you cloned the Git repo into, **Open** the file **R/Notebooks/EDA_Analysis.Rmd**
2. At the top of the code file, within the preamble (yml), change the variable **is.docker** to look like **is.docker <- FALSE**.
3. Within the **Configuration** section of the code file, edit the variable **database** to the location of the your local copy of the SQLite database. 
4. Set your working directory by clicking **Session --> Set Working Directory --> To Source File Location**
5. Click the **Knit** button in the upper-left part of the screen (~ 15 minutes on a single-core/high-memory machine)
6. After completion of execution, an HTML file called **EDA_Analysis.html** will be placed in the **R/Notebooks/** directory and can be viewed in a browser of your choice.

##### Executing Differential Analysis Code

1. Click **File --> Open File** and navigate the directory you cloned the Git repo into, **Open** the file **R/Notebooks/Differential_Analysis.Rmd**
2. At the top of the code file, within the preamble (yml), change the variable **is.docker** to look like **is.docker <- FALSE**.
3. Within the **Configuration** section of the code file, edit the variable **database** to the location of the your local copy of the SQLite database. 
4. Set your working directory by clicking **Session --> Set Working Directory --> To Source File Location**
5. Click the **Knit** button in the upper-left part of the screen (~ 15 minutes on a single-core/high-memory machine)
6. After completion of execution, an HTML file called **Differential_Analysis.html** will be placed **R/Notebooks** directory and it can be viewed using your browser of choice. In addition, a directory called **reports** will be present which contains a series of HTML documents which contain the results from a differential analysis performed via DESeq2.

#### Executing Python-based Analysis

##### Install Python Libraries

We recommend using **conda** to create your Python environment if not using Docker.

1. Install libraries specified in the **Python/clustering/requirements.txt** file. This can be done using a **conda** environment like so if you so choose:

   ```bash
   conda create --name npars_clustering --file </path/to/requirements.txt>
   conda activate npars_clustering
   ```

##### Execute Python-based Code

1. Execute Jupyter Notebook and browse to the URL displayed at the command prompt. For example, on a Linux-based machine one can run:

   ```
   jupyter notebook
   ```

2. Navigate to the location of the cloned GitLab repo and open the file **Python/clustering/Notebooks/Clustering.ipynb**

3. Edit Cell #7 by replacing **/src/Data/MockGMU_20.db** with the location of your local **/path/to/sqlite/directory/MockGMU_20.db**

4. Click on **Cell --> Run All** This will run all Python-based analysis code blocks and display the results within the notebook.



## Full Guide

In this section, we present more thorough guide, going over how one would import your own data into an SQLite DB for analysis. 

### Import File Specifications

In order to import one's own data into an SQLite database for analysis, is to prepare the input files required. Here we present all of the files one will need to have in standardized files, in order to run the included Python-based importation script and create an SQLite database from that data.

The files used to create the example database used in the previous **Quick Start** sections can be downloaded as a tar.gz from the following link (via a web browser): [download](https://drive.google.com/file/d/1XFdpZHRfppn6jSKHeA9T161Rp9o03zvY/view?usp=sharing).

**OR**

Use the following commands can be used on a Linux-based command line:

```bash
fileId=1XFdpZHRfppn6jSKHeA9T161Rp9o03zvY

fileName=example-data.tar.gz

curl -sc /tmp/cookie "https://drive.google.com/uc?export=download&id=${fileId}" > /dev/null

code="$(awk '/_warning_/ {print $NF}' /tmp/cookie)"

curl -Lb /tmp/cookie "https://drive.google.com/uc?export=download&confirm=${code}&id=${fileId}" -o ${fileName}
```

Each file is listed with its purpose, name, column specification, and example data.

#### Merged GTF

The first file that is needed is a "merged GTF" file, that contains all transcript annotations from all the RNA-seq experiments in your cohort. We use the merged GTF which is generated by the _merge_ mode of the tool _StringTie_. Refer to GTF specification.

**Example** - Merged GTF

**output_merged.gtf**

```
1       StringTie       transcript      11869   14409   1000    +       .       gene_id "MSTRG.1"; transcript_id "ENST00000456328"; gene_name "DDX11L1"; ref_gene_id "ENSG00000223972";
1       StringTie       exon    11869   12227   1000    +       .       gene_id "MSTRG.1"; transcript_id "ENST00000456328"; exon_number "1"; gene_name "DDX11L1"; ref_gene_id "ENSG00000223972";
1       StringTie       exon    12613   12721   1000    +       .       gene_id "MSTRG.1"; transcript_id "ENST00000456328"; exon_number "2"; gene_name "DDX11L1"; ref_gene_id "ENSG00000223972";
1       StringTie       exon    13221   14409   1000    +       .       gene_id "MSTRG.1"; transcript_id "ENST00000456328"; exon_number "3"; gene_name "DDX11L1"; ref_gene_id "ENSG00000223972";
```

------

#### Ensembl Static Lookup Files

Three static lookup tables are needed: 1) one which has data on gene ids and gene symbols; 2) one containing cytoband data; and 3) the last containing a many-to-many relationship between the previous two.

Pre-built Ensembl GRCh19 related files can be downloaded [here](placeholder.com):

##### hg19 Ensembl Genes Lookup

| Column Name     | Data Type             |
| --------------- | --------------------- |
| id              | integer - primary key |
| chrom           | string                |
| start           | integer               |
| end             | integer               |
| ensembl_gene_id | string                |
| gene_symbol     | string                |

**Example** - hg19 Ensembl Genes Lookup

**hg19EnsemblGeneIDs.csv**

```
1,8,128747680,128753674,ENSG00000136997,MYC
2,12,25357723,25403870,ENSG00000133703,KRAS
3,17,7565097,7590856,ENSG00000141510,TP53
```

##### hg19 Ensembl Genes Lookup

| Column Name | Data Type             |
| ----------- | --------------------- |
| id          | integer - primary key |
| chrom       | string                |
| start       | integer               |
| end         | integer               |
| band        | string                |

**Example** - hg19 Ensembl Bands Lookup

**hg19EnsemblBands.csv**

```
1,8,127300001,131500000,8q24.21
2,12,21300001,26500000,12p12.1
3,17,6500001,10700000,17p13.1
```

##### hg19 Ensemble Genes & Bands Lookup

| Column Name           | Data Type                                                    |
| --------------------- | ------------------------------------------------------------ |
| hg19EnsemblGeneIDs_ID | integer - primary key and foreign key to **hg19EnsemblGeneIDs.csv** |
| hg19EnsemblBands_ID   | integer - primary key and foreign key to **hg19EnsemblBands.csv** |

**Example** - hg19 Ensembl Genes & Bands Lookup

**hg19EnsemblBandsToGeneIDs.csv**

```
1,1
2,2
3,3
```

------

#### Metadata for Bioinformatic Experiment

This file contains basic metadata regarding the experiments that are to be imported.

| Column Name          | Data Type                    |
| -------------------- | ---------------------------- |
| bioinfoexperiment_id | integer - primary key        |
| study_type           | string (RNA-Seq, Panel, WGS) |
| category             | string                       |

**Example** - Metadata for Bioinformatic Experiment

**metadata_bioinfoexperiment.csv**

```
1,RNA-seq,category1
2,WGS,category2
3,Panel,category3
4,RNA-seq,category1
5,RNA-seq,category1
```

------

#### Copy Number Variation Data 

This file contains output from a copy number variation experiment in which the regions of similar copy number variation has been "segmented". The output below is indicative of the output from the tool _ichorCNA_.

| Column Name           | Data Type                                                   |
| --------------------- | ----------------------------------------------------------- |
| id                    | integer - primary key                                       |
| chrom                 | string                                                      |
| start                 | integer                                                     |
| end                   | integer                                                     |
| copy_number           | integer                                                     |
| segmented_median_logR | floating point number                                       |
| subclone_status       | string                                                      |
| call                  | string                                                      |
| bioinfoexperiment_id  | integer - foreign key to **metadata_bioinfoexperiment.csv** |

**Example** - Copy Number Variation Data

**cnv_segmented.csv**

```
1,4,2000001,191000000,2,0.00,False,NEUT,1
2,5,1000001,180000000,3,0.30,True,GAIN,1
3,6,1000001,170000000,2,0.00,False,NEUT,1
```

------

#### DNA Mutations Metadata

This file contains metadata for DNA mutations to be imported from the **dna_mutations.csv** later described.

| Column Name          | Data Type                                                    |
| -------------------- | ------------------------------------------------------------ |
| study_id             | integer - primary key                                        |
| tumor_germline       | string                                                       |
| bioinfoexperiment_id | integer - foreign key to **metadata_bioinforexperiment.csv** |

**Example** - DNA Mutations Metadata

**dna_mutations_metadata.csv**

```
1,Tumor,1
2,Germline,2
3,Tumor,3
```

------

#### DNA Mutations

This file contains DNA mutational data.

| Column Name | Data Type                                               |
| ----------- | ------------------------------------------------------- |
| gene_id     | string                                                  |
| gene_name   | string                                                  |
| chrom       | string                                                  |
| pos         | integer                                                 |
| ref         | string                                                  |
| alt         | string                                                  |
| annotation  | string                                                  |
| filter      | string                                                  |
| allele_freq | floating point number                                   |
| dp          | integer                                                 |
| study_id    | integer - foreign key to **dna_mutations_metadata.csv** |

**Example** - DNA Mutations

**dna_mutations.csv**

```
ENSG00000181143,MUC16,chr19,9020052,G,A,synonymous_variant,Germline_Risk,0.2,4693,2
ENSG00000168702,LRP1B,chr2,141274576,T,C,synonymous_variant,PASS,0.7,1205,1
ENSG00000157404,KIT,chr4,55593464,A,C,missense_variant,PASS,0.834,1630,3
```

------

#### RNA Expressed Mutations

This file contains RNA expressed mutation data.

| Column Name          | Data Type                                                   |
| -------------------- | ----------------------------------------------------------- |
| id                   | integer - primary key                                       |
| chrom                | string                                                      |
| pos                  | integer                                                     |
| ref                  | string                                                      |
| alt                  | string                                                      |
| variant_type_id      | integer (1 = SNP, 2 = InDel)                                |
| pass_filter          | string (True = PASS, False = FAIL)                          |
| tumor_dp             | int                                                         |
| tumor_af             | floating point number                                       |
| bioinfoexperiment_id | integer - foreign key to **metadata_bioinfoexperiment.csv** |

**Example** - RNA Expressed Mutations

**rna_expressed_mutations.csv**

```
1,1,27533505,G,T,1,True,7.0,0.42,1
2,1,27533577,A,G,1,True,8.0,0.5,1
3,1,28422441,C,T,1,True,2.0,1.0,1
```

------

#### RNA Expressed Mutations Annotations

This file contains annotations related to the data in **rna_expressed_mutations.csv** file.

| Column Name     | Data Type                                                |
| --------------- | -------------------------------------------------------- |
| id              | integer - primary key                                    |
| gene_name       | string                                                   |
| ensembl_gene_id | string                                                   |
| effect_impact   | string                                                   |
| rna_exp_mut_id  | integer - foreign key to **rna_expressed_mutations.csv** |

**Example** - RNA Expressed Mutations Annotations

**rna_expressed_mutations_annotations.csv**

```
1,RPL5P4,ENSG00000229994,MODIFIER,1
2,INPP5B,ENSG00000204084,HIGH,1
3,INPP5B,ENSG00000204084,HIGH,2
```

------

#### RNA Gene Abundances

This file contains gene-level abundances, as well as, other information about each gene. This data was generated using _StringTie_ with the -A flag.

| Column Name          | Data Type                                                   |
| -------------------- | ----------------------------------------------------------- |
| id                   | integer - primary key                                       |
| gene_id              | integer                                                     |
| gene_name            | string                                                      |
| reference            | string                                                      |
| strand               | string                                                      |
| start                | integer                                                     |
| end                  | integer                                                     |
| coverage             | floating point number                                       |
| fpkm                 | floating point number                                       |
| tpm                  | floating point number                                       |
| bioinfoexperiment_id | integer - foreign key to **metadata_bioinfoexperiment.csv** |

**Example** - RNA Gene Abundances

**rna_gene_abundances.csv**

```
1,ENSG00000144847,IGSF11,3,-,118619404,118864915,0.0,0.0,0.0,1
2,ENSG00000239877,IGSF11-AS1,3,+,118661920,118667088,0.0,0.0,0.0,1
3,ENSG00000163424,C3orf30,3,+,118864997,118878889,1.100000023841858,0.6000000238418579,0.8999999761581421,1
```

------

#### RNA Gene Counts

This file contains gene-level raw counts for an RNA-seq experiment. This data was generated using _StringTie_ using the -e and -B flags, and then importing that data into _IsoformSwitchAnalyzeR_ to cleanup annotations, and to export to the tab-separated file below.

**N.B. This table is a bit different than the rest. Here the file is a tab-separated file. The first row is made up of the sample name(s) in the experiment (where the sample name is suffixed with '_<bioinfoexperiment_id>'), and the last column is the gene_id.**

| Column Name | Data Type             |
| ----------- | --------------------- |
| sample_1    | floating point number |
| sample_2    | floating point number |
| ...         |                       |
| sample_n    | floating point number |
| gene_id     | string                |

**Example** - RNA Gene Counts

**gene_quant.csv**

```
RNA_TUMOR_1	RNA_NL_4	RNA_NL_5	gene_id
262.344713040934	580.569236869036	858.601384635059	ENSG00000000003
0	0	0	ENSG00000000005
483.257837791607	1006.1265922773	1410.43384544028	ENSG00000000419
```

------

#### RNA Gene Fusions

| Column Name          | Data Type                                                   |
| -------------------- | ----------------------------------------------------------- |
| id                   | integer - primary key                                       |
| left_breakpoint      | integer                                                     |
| right_breakpoint     | integer                                                     |
| left_chromosome      | string                                                      |
| right_chromosome     | string                                                      |
| bioinfoexperiment_id | integer - foreign key to **metadata_bioinfoexperiment.csv** |

**Example** - RNA Gene Fusions

**rna_gene_fusions.csv**

```
1,64430159,64184643,chr3,chr3,1
2,31908813,33213011,chr5,chr21,1
3,64184643,64430159,chr3,chr3,1
```

------

#### Study Design

This file contains information regarding the design of the RNA-seq experiments to be run via DESeq2. This is a tab-separated file as well.

**study_design.txt**

| Column Name | Data Type |
| ----------- | --------- |
| sample      | string    |
| condition   | string    |
| patient     | string    |
| shortname   | string    |
| data_color  | string    |

**Example** - Study Design

```
sample	condition	patient	shortname	data_color
RNA_TUMOR_1	Tumor	John_Smith	RNA_1	darkred
RNA_NL_4	Normal	John_Smith	RNA_4	darkblue
RNA_NL_5	Normal	John_Smith	RNA_5	black
```

### Running Importer: Using Docker

In this section, we will describe the steps to use the Docker-based importer to import the files listed in the **Import File Specifications** section.

#### Prerequisites

1. [Docker Engine](https://docs.docker.com/engine/install/) and [Docker Compose](https://docs.docker.com/compose/install/). Follow instructions to give non-root users the ability to run the two aforementioned tools. If not, you must prefix all Docker commands with **sudo**.
2. [Git](https://git-scm.com/)

#### Installation

1. Clone GitLab repo:

   ```bash
   git clone https://gitlab.com/erichpeterson/npars-analysis.git
   ```

2. Create a directory that will store the SQLite database and allow other permission to write on that directory.

   For example, on Linux-based machines:

   ```bash
   chmod 766 /path/to/sqlite/directory/
   ```

3. Place all file adhering to the **Import File Specifications** in the same directory created in the previous step. 

4. Change directories to the directory in which the GitLab repo was cloned to.

5. Open the file **docker-compose.yml** and under the **npars-importer**, edit the first half of the string (i.e., the half before the colon **:**) to the path defined to have the database file in the **volumes** section.

6. Run the Docker Compose command below to build the Docker images (will take ~ 30-40 minutes).

   ```bash
   docker-compose build --force-rm --no-cache --parallel
   ```

#### Run Importer

1. Run the Docker containers using Docker Compose:

   ```bash
   docker-compose up
   ```

2. Open a new command line terminal, change directories to the directory you cloned the GitLab repo to and issue the following command:

   ```bash
   docker-compose exec npars-importer /bin/bash
   ```

3. You will be presented with a Bash prompt inside the Docker container.

4. Issue the following commands, replacing **<sqlite_database_name>.db** with the name of your preferred SQLite database file name:

   ```bash
   cd /src/Data
   mv gene_quant.csv origin_gene_quant.csv
   python3 /src/Scripts/IsoformSwitchTransformer.py --input origin_gene_quant.csv --output gene_quant.csv
   python3 /src/Scripts/SqliteImport.py --all <sqlite_database_name>.db
   exit
   ```

5. Open the terminal which executed the **docker-compose up** command, and stop the containers by typing **CTRL-C**, and then remove the containers with the following command:

   ```bash
   docker-compose down
   ```

6. Navigate to **/path/to/sqlite/directory/** and change ownership and permissions to the newly created SQLite database:

   ```bash
   sudo chown <username>:<group> <sqllite_database_name>.db
   chmod 766 <sqlite_database_name>.db
   ```

7. One can now run the R and Python-based analyses using the instructions found in the **Quick Start** guide starting at the **Running Software** Section, with the exception of the following:

   1. You will need to replace the **database** variable in the **Configuration Section** of the **EDA_Analysis.Rmd**, **Differential_Analysis.Rmd**, and **Clustering.ipynb** to the path of your **<sqlite_database_name>.db**

   2. You will need to change the **contrast_ad** and the **case_grp** and **control_grp** variables in the **Configuration Section** of **Differential_Analysis.Rmd** to conform to your RNA-seq differential analysis comparison. For example, if one has two samples (of condition: Case/Control), with three replicates each (named Case-1, Case-2, Case-3, Control-1, Control-2, Control-3), the following would an example of the variables:

      ```R
      contrast_ad <- c("condition", "Case", "Control")
      case_grp <- c("Case-1", "Case-2", "Case-3")
      control_grp <- c("Control-1", "Control-2", "Control-3")
      ```

      

### Running Importer: Not Using Docker

#### Prerequisites

1. [Python](https://www.python.org/) v3
2. [Git](https://git-scm.com/)

#### Installation

1. Clone GitLab repo:

   ```bash
   git clone https://gitlab.com/erichpeterson/npars-analysis.git
   ```

2. Create a directory that will store the SQLite database and allow all write permissions on that directory.

   For example, on Linux-based machines:

   ```bash
   chmod 766 /path/to/sqlite/directory/
   ```

3. Place all file adhering to the **Import File Specifications** in the same directory created in the previous step. 

4. Rename file **/path/to/sqlite/directory/gene_quant.csv** to **path/to/sqlite/directory/origin_gene_quant.csv**

#### Running Software

1. Change directories to **/path/to/sqlite/directory**.

2. Issue the following commands, replacing **<sqlite_database_name>.db** with the name of your preferred SQLite database file name and **/path/to/cloned/repo/** with the directory you cloned the code to:

3. 

   ```bash
   python3 </path/to/cloned/repo/>Python/import_scripts/Scripts/IsoformSwitchTransformer.py --input origin_gene_quant.csv --output gene_quant.csv
   python3 </path/to/cloned/repo/>Python/import_scripts/Scripts/SqliteImport.py --all <sqlite_database_name>.db
   ```

4. Navigate to **/path/to/sqlite/directory/** and change ownership and permissions to the newly created SQLite database:

   ```bash
   sudo chown <username>:<group> <sqllite_database_name>.db
   chmod 766 <sqlite_database_name>.db
   ```

5. One can now run the R and Python-based analyses using the instructions found in the **Quick Start** guide starting at the **Running Software** Section, with the exception of the following:

   1. You will need to replace the **database** variable in the **Configuration Section** of the **EDA_Analysis.Rmd**, **Differential_Analysis.Rmd**, and **Clustering.ipynb** to the path of your **<sqlite_database_name>.db**

   2. You will need to change the **contrast_ad** and the **case_grp** and **control_grp** variables in the **Configuration Section** of **Differential_Analysis.Rmd** to conform to your RNA-seq differential analysis comparison. For example, if one has two samples (of condition: Case/Control), with three replicates each (named Case-1, Case-2, Case-3, Control-1, Control-2, Control-3), the following would an example of the variables:

      ```R
      contrast_ad <- c("condition", "Case", "Control")
      case_grp <- c("Case-1", "Case-2", "Case-3")
      control_grp <- c("Control-1", "Control-2", "Control-3")
      ```

### Example of Importing Custom Data 

In this section, a **real example** is given on how to import custom data into an SQLite database and use that data to perform an EDA and Differential Analysis on RNA-seq expression data.

This example contains two samples (case/control), with each sample having three replicates.

#### Preparing Data for Import

To use example data, download the dataset, as well as, the other required files to perform the aforementioned analysis here: [download](https://drive.google.com/file/d/1MfPxJhcPLi3qqqqFY7W64oSdE_3MxNFz/view?usp=sharing)

**OR**

Use the following commands can be used on a Linux-based command line:		

```bash
fileId=1MfPxJhcPLi3qqqqFY7W64oSdE_3MxNFz

fileName=example-data-rna-seq.tar.gz

curl -sc /tmp/cookie "https://drive.google.com/uc?export=download&id=${fileId}" > /dev/null

code="$(awk '/_warning_/ {print $NF}' /tmp/cookie)"

curl -Lb /tmp/cookie "https://drive.google.com/uc?export=download&confirm=${code}&id=${fileId}" -o ${fileName}
```

#### Example File Contents

Here we disseminate the contents of the files contained within the .tar.gz downloaded.

**See full file specifications in the previous section.**

#### Metadata for Bioinformatic Experiment

**Example** - Metadata for Bioinformatic Experiment

**metadata_bioinfoexperiment.csv**

```
172557,RNA-Seq,Case
172558,RNA-Seq,Case
172559,RNA-Seq,Case
172563,RNA-Seq,Control
172564,RNA-Seq,Control
172565,RNA-Seq,Control
```

------

#### Study Design

**Example** - Study Design

**study_design.txt**

```
sample  condition       patient shortname       data_color
SYN-GM-20_172557        Case    SYN-GM-20       Case-1  darkred
SYN-GM-20_172558        Case    SYN-GM-20       Case-2  darkred
SYN-GM-20_172559        Case    SYN-GM-20       Case-3  darkred
SYN-GM-20_172563        Control SYN-GM-20       Control-1       black
SYN-GM-20_172564        Control SYN-GM-20       Control-2       black
SYN-GM-20_172565        Control SYN-GM-20       Control-3       black
```

------

#### RNA Gene Counts

**Example** - RNA Gene Counts (abbreviated)

**gene_quant.csv**

```
SYN-GM-20_172557 SYN-GM-20_172558 SYN-GM-20_172559 SYN-GM-20_172563 SYN-GM-20_172564 SYN-GM-20_172565 gene_id
262.344713040934 580.569236869036 858.601384635059 1040.89777724246 1150.33248651016 623.432043417809 ENSG00000000003
0 0 0 0 0 0 ENSG00000000005
483.257837791607 1006.1265922773 1410.43384544028 1691.56476019567 1823.19119209078 1181.06757945153 ENSG00000000419
```

------

In addition to the files above, the .tar.gz contains the **Merged GTF** and **Ensembl Static Lookup Tables**.

#### Importing Data

To import the example, simply follow the directions found in [Running Importer: Using Docker](#running-importer-using-docker) or [Running Importer: Not Using Docker](#running-importer-not-using-docker), depending on your environment.



## Development In-Progress

NPARS was developed with extensibility in mind, and as such new features are always being added. Below are listed some on-going developments.

- Add strand and distance to nearest gene in EDA Analysis for novel genes.
- Add self-organizing maps (SOM), per our interest in unsupervised machine learning algorithms.



## Cite

Currently in-review

