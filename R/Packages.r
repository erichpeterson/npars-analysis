#readr
install.packages("readr")
#bioconductor
if (!requireNamespace("BiocManager", quietly = TRUE))
  install.packages("BiocManager")
#tximport
BiocManager::install("tximport", force = TRUE)
#DESeq2
BiocManager::install("DESeq2", force = TRUE)
#pheatmap
install.packages("pheatmap")
#RColorBrewer
install.packages("RColorBrewer")
#PoiClaClu
install.packages("PoiClaClu")
#ggplot2
install.packages("ggplot2")
#gplots
install.packages("gplots")
#biomaRt
BiocManager::install("biomaRt", force = TRUE)
#GGally
install.packages("GGally")
#reshape2
install.packages("reshape2")
#psych
install.packages("psych")
#knitr
install.packages("knitr")
#kableExtra
install.packages("kableExtra")
#RSQLite
install.packages("RSQLite")
#htmltools
install.packages("htmltools")
#reactable
install.packages("reactable")
#png
install.packages("png")
#ggplotify
install.packages("ggplotify")

#rhdf5
BiocManager::install("rhdf5", force = TRUE)
#GenomicFeatures
BiocManager::install("GenomicFeatures", force = TRUE)
#tidyverse
install.packages("tidyverse")
#AnnotationDbi
BiocManager::install("AnnotationDbi", force = TRUE)
#GetoptLong
install.packages("GetoptLong")
#formattable
install.packages("formattable")
#DT
install.packages("DT")
#RCircos
install.packages("RCircos")
#gridGraphics
install.packages("gridGraphics")
#gridExtra
install.packages("gridExtra")
#kohonen
install.packages("kohonen")

#ReportingTools
BiocManager::install("ReportingTools", force = TRUE)
#gprofiler2
install.packages("gprofiler2")
#gProfileR
install.packages("gProfileR")
#fdrtool
install.packages("fdrtool")
#rtracklayer
BiocManager::install("rtracklayer", force = TRUE)



#Biobase
BiocManager::install("Biobase", force = TRUE)
#RCurl
BiocManager::install("RCurl", force = TRUE)
#Rcpp
install.packages("Rcpp")
#hash
install.packages("hash")
#ggvenn
install.packages("ggvenn")
#plyr
install.packages("plyr")
