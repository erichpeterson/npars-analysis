import pickle, sqlite3, os, sys, chardet, csv
import pandas as pd
from pathlib import Path
from gtfparse import read_gtf
class DBOperator():

    def __init__(self, dbName):
        self.__csvFilesDict = {"--rna_genefusions": "rna_gene_fusions.csv",
                               "--rna_expmut": "rna_expressed_mutations.csv",
                               "--rna_expmutann": "rna_expressed_mutations_annotations.csv",
                               "--cnv_segmented": "cnv_segmented.csv",
                               "--dna_mutmetadata": "dna_mutations_metadata.csv",
                               "--dna_mutations": "dna_mutations.csv",
                               "--rna_geneabundances": "rna_gene_abundances.csv",
                               "--rna_genecounts": "gene_quant.csv",
                               "--gtf": "output_merged.gtf",
                               "--hg19Bands": "hg19EnsemblBands.csv",
                               "--hg19BandsToGeneIDs": "hg19EnsemblBandsToGeneIDs.csv",
                               "--hg19GeneIDs": "hg19EnsemblGeneIDs.csv",
                               "--metaBioexp": "metadata_bioinfoexperiment.csv",
                               "--studyDesign": "study_design.txt",
                               "--methyldackel": "methyldackel.csv"
                               }
        self.__tableNamesDict = {"--rna_genefusions": "RNA_GeneFusions",
                                 "--rna_expmut": "RNA_ExpressedMutations",
                                 "--rna_expmutann": "RNA_ExpMutAnnotations",
                                 "--cnv_segmented": "CNV_Segmented",
                                 "--dna_mutmetadata": "DNA_MutationsMetaData",
                                 "--dna_mutations": "DNA_Mutations",
                                 "--rna_geneabundances": "RNA_GeneAbundances",
                                 "--rna_genecounts": "RNA_GeneCounts",
                                 "--gtf": "Merged_GTF",
                                 "--hg19Bands": "hg19EnsemblBands",
                                 "--hg19BandsToGeneIDs": "hg19EnsemblBandsToGeneIDs",
                                 "--hg19GeneIDs": "hg19EnsemblGeneIDs",
                                 "--metaBioexp": "MetaBioinfoexperiment",
                                 "--studyDesign": "StudyDesign",
                                "--methyldackel": ["Methyldackel", "MethylkitMetadata", "MethylkitMetadataBioinfoexperiment", "MethylkitDMRAnnotationType", "MethylkitDMR", "MethylkitDMRAnnotation"]
                                }
        self.__colNamesDict = {
                                "--rna_genefusions": ['id',
                                            'left_breakpoint',
                                            'right_breakpoint',
                                            'left_chromosome',
                                            'right_chromosome',
                                            'bioinfoexperiment_id'
                                            ],
                                "--rna_expmut": ['id',
                                            'chrom',
                                            'pos',
                                            'ref',
                                            'alt',
                                            'variant_type_id',
                                            'pass_filter',
                                            'tumor_dp',
                                            'tumor_af',
                                            'bioinfoexperiment_id'
                                            ],
                                "--rna_expmutann": ['id',
                                            'gene_name',
                                            'ensembl_gene_id',
                                            'effect_impact',
                                            'rna_exp_mut_id'
                                            ],
                                "--cnv_segmented": ['id',
                                            'chrom',
                                            'start',
                                            'end',
                                            'copy_number',
                                            'segmented_median_logR',
                                            'subclone_status',
                                            'call',
                                            'bioinfoexperiment_id'
                                            ],
                                "--dna_mutmetadata": ['study_id',
                                            'tumor_germline',
                                            'bioinfoexperiment_id'
                                            ],
                                "--dna_mutations": ['gene_id',
                                            'gene_name',
                                            'chrom',
                                            'pos',
                                            'ref',
                                            'alt',
                                            'annotation',
                                            'filter',
                                            'allele_freq',
                                            'dp',
                                            'study_id'
                                            ],
                                "--rna_geneabundances": ['id',
                                            'gene_id',
                                            'gene_name',
                                            'reference',
                                            'strand',
                                            'start',
                                            'end',
                                            'coverage',
                                            'fpkm',
                                            'tpm',
                                            'bioinfoexperiment_id'
                                            ],
                                "--rna_genecounts": ['bioinfoexperiment_id',
                                	      	'gene_id',
                                	      	'count'
						                    ],
                                "--gtf": ['id',
                                	'chrom',
                                	'start',
                                	'end',
                                	'strand',
                                	'source',
                                	'type',
                                	'gene_id',
                                	'transcript_id',
                                	'gene_name',
                                	'ref_gene_id'
                                	   ],
                                "--hg19Bands":[
                                    'id',
                                    'chrom',
                                    'start',
                                    'end',
                                    'band'
                                    ],
                                "--hg19GeneIDs": [
                                    'id',
                                    'chrom',
                                    'start',
                                    'end',
                                    'ensembl_gene_id',
                                    'gene_symbol'
                                ],
                                "--hg19BandsToGeneIDs": [
                                    'hg19EnsemblGeneIDs_ID',
                                    'hg19EnsemblBands_ID'
                                ],
                                "--metaBioexp": [
                                    'bioinfoexperiment_id',
                                    'study_type',
                                    'category'
                                ],
                                "--studyDesign": [
                                    'sample',
                                    'condition',
                                    'patient',
                                    'shortname',
                                    'data_color'
                                ],
                                "--methyldackel": [[
                                    'id',
                                    'chrbase',
                                    'chr',
                                    'base',
                                    'strand',
                                    'coverage',
                                    'freqC',
                                    'freqT',
                                    'bioinfoexperiment_id'
                                ],
                                [
                                    'id',
                                    'description',
                                    'study_design'
                                ],
                                [
                                    'methylkit_metadata_id',
                                    'methylkit_bioinfoexperiment_id'
                                ],
                                [
                                    'id',
                                    'description'
                                ],
                                [
                                    'id',
                                    'chr',
                                    'start',
                                    'end',
                                    'width',
                                    'strand',
                                    'pvalue',
                                    'qvalue',
                                    'diff_meth_perc',
                                    'methylkit_metadata_id'
                                ],
                                [
                                    'id',
                                    'chr',
                                    'start',
                                    'end',
                                    'width',
                                    'strand',
                                    'annotatr_annot_id',
                                    'ucsc_gene_id',
                                    'entrez_gene_id',
                                    'ensembl_gene_id',
                                    'gene_symbol',
                                    'annotatr_type_db',
                                    'methylkit_dmr_id',
                                    'annotation_type_id'
                                ]]
                            }
        self.__schemaDict = {
                            "--metaBioexp": '''
                                CREATE TABLE MetaBioinfoexperiment(
                                    bioinfoexperiment_id INTEGER PRIMARY KEY NOT NULL,
                                    study_type TEXT NOT NULL,
                                    category TEXT NULL
                                )
                            ''',
                            "--rna_genefusions": '''
                                CREATE TABLE RNA_GeneFusions(
                                    id INTEGER PRIMARY KEY NOT NULL,
                                    left_breakpoint INTEGER NOT NULL,
                                    right_breakpoint INTEGER NOT NULL,
                                    left_chromosome TEXT NOT NULL,
                                    right_chromosome TEXT NOT NULL,
                                    bioinfoexperiment_id,
                                    FOREIGN KEY(bioinfoexperiment_id) REFERENCES MetaBioinfoexperiment(bioinfoexperiment_id)
                                )
                            ''',
                            "--rna_expmut": '''
                                CREATE TABLE RNA_ExpressedMutations(
                                    id INTEGER PRIMARY KEY NOT NULL,
                                    chrom TEXT NOT NULL,
                                    pos INTEGER NOT NULL,
                                    ref TEXT,
                                    alt TEXT,
                                    variant_type_id INTEGER NOT NULL,
                                    pass_filter INTEGER NOT NULL,
                                    tumor_dp REAL NOT NULL,
                                    tumor_af REAL NOT NULL,
                                    bioinfoexperiment_id INTEGER NOT NULL,
                                    FOREIGN KEY(bioinfoexperiment_id) REFERENCES MetaBioinfoexperiment(bioinfoexperiment_id)
                                )
                            ''',
                            "--rna_expmutann": '''
                                CREATE TABLE RNA_ExpMutAnnotations(
                                    id INTEGER PRIMARY KEY NOT NULL,
                                    gene_name TEXT,
                                    ensembl_gene_id TEXT,
                                    effect_impact TEXT,
                                    rna_exp_mut_id INTEGER,
                                    FOREIGN KEY(rna_exp_mut_id) REFERENCES RNA_ExpressedMutations(id)
                                )
                            ''',
                            "--cnv_segmented": '''
                                CREATE TABLE CNV_Segmented(
                                    id INTEGER PRIMARY KEY NOT NULL,
                                    chrom TEXT NOT NULL,
                                    start INTEGER NOT NULL,
                                    end INTEGER NOT NULL,
                                    copy_number INTEGER NOT NULL,
                                    segmented_median_logR REAL NOT NULL,
                                    subclone_status INTEGER NOT NULL,
                                    call TEXT NOT NULL,
                                    bioinfoexperiment_id INTEGER NOT NULL,
                                    FOREIGN KEY(bioinfoexperiment_id) REFERENCES MetaBioinfoexperiment(bioinfoexperiment_id)
                                )
                            ''',
                            "--dna_mutmetadata": '''
                                CREATE TABLE DNA_MutationsMetaData(
                                    study_id INTEGER PRIMARY KEY NOT NULL,
                                    tumor_germline TEXT,
                                    bioinfoexperiment_id INTEGER,
                                    FOREIGN KEY(bioinfoexperiment_id) REFERENCES MetaBioinfoexperiment(bioinfoexperiment_id)
                                )
                            ''',
                            "--dna_mutations": '''
                                CREATE TABLE DNA_Mutations(
                                    gene_id TEXT,
                                    gene_name TEXT,
                                    chrom TEXT,
                                    pos INT,
                                    ref TEXT,
                                    alt TEXT,
                                    annotation TEXT,
                                    filter TEXT,
                                    allele_freq REAL,
                                    dp INT,
                                    study_id INT,
                                    FOREIGN KEY(study_id) REFERENCES DNA_MutationsMetaData(study_id)
                                )
                            ''',
                            "--rna_geneabundances": '''
                                CREATE TABLE RNA_GeneAbundances(
                                    id INTEGER PRIMARY KEY NOT NULL,
                                    gene_id TEXT NOT NULL,
                                    gene_name TEXT NOT NULL,
                                    reference TEXT NOT NULL,
                                    strand TEXT NOT NULL,
                                    start INTEGER NOT NULL,
                                    end INTEGER NOT NULL,
                                    coverage REAL NOT NULL,
                                    fpkm REAL NOT NULL,
                                    tpm REAL NOT NULL,
                                    bioinfoexperiment_id INTEGER NOT NULL,
                                    FOREIGN KEY(bioinfoexperiment_id) REFERENCES MetaBioinfoexperiment(bioinfoexperiment_id)
                                )
                            ''',
                            "--rna_genecounts": '''
                            	 CREATE TABLE RNA_GeneCounts(
                            	     bioinfoexperiment_id INTEGER NOT NULL,
                            	     gene_id TEXT NOT NULL,
                            	     count REAL NOT NULL,
                                     FOREIGN KEY(bioinfoexperiment_id) REFERENCES MetaBioinfoexperiment(bioinfoexperiment_id)
                            	 )
                            ''',
                            "--gtf": '''
                            	CREATE TABLE Merged_GTF(
                            	    id INTEGER PRIMARY KEY NOT NULL,
                            	    chrom TEXT NOT NULL,
                            	    start INTEGER NOT NULL,
                            	    end INTEGER NOT NULL,
                            	    strand TEXT NOT NULL,
                            	    source TEXT NOT NULL,
                            	    type TEXT NOT NULL,
                            	    gene_id TEXT NOT NULL,
                            	    transcript_id TEXT NOT NULL,
                            	    gene_name TEXT,
                            	    ref_gene_id TEXT
                            	)
                            ''',
                            "--hg19Bands": '''
                                CREATE TABLE hg19EnsemblBands(
                                    id INTEGER PRIMARY KEY NOT NULL,
                                    chrom INTEGER NOT NULL,
                                    start INTEGER NOT NULL,
                                    end INTEGER NOT NULL,
                                    band TEXT NOT NULL
                                )
                            ''',
                            "--hg19GeneIDs": '''
                                CREATE TABLE hg19EnsemblGeneIDs(
                                    id INTEGER PRIMARY KEY NOT NULL,
                                    chrom INTEGER NOT NULL,
                                    start INTEGER NOT NULL,
                                    end INTEGER NOT NULL,
                                    ensembl_gene_id TEXT NOT NULL,
                                    gene_symbol TEXT NOT NULL
                                )
                            ''',
                            "--hg19BandsToGeneIDs": '''
                                CREATE TABLE hg19EnsemblBandsToGeneIDs(
                                    hg19EnsemblGeneIDs_ID INTEGER NOT NULL,
                                    hg19EnsemblBands_ID INTEGER NOT NULL,
                                    PRIMARY KEY(hg19EnsemblGeneIDs_ID, hg19EnsemblBands_ID),
                                    FOREIGN KEY(hg19EnsemblGeneIDs_ID) REFERENCES hg19EnsemblGeneIDs(id),
                                    FOREIGN KEY(hg19EnsemblBands_ID) REFERENCES hg19EnsemblBands(id)
                                )
                            ''',
                            "--studyDesign": '''
                                CREATE TABLE StudyDesign(
                                    sample TEXT PRIMARY KEY NOT NULL,
                                    condition TEXT NOT NULL,
                                    patient TEXT NOT NULL,
                                    shortname TEXT NOT NULL,
                                    data_color TEXT NOT NULL
                                )
                            ''',
                            "--methyldackel": ['''
                                CREATE TABLE Methyldackel(
                                    id INTEGER PRIMARY KEY,
                                    chrbase TEXT NOT NULL,
                                    chr TEXT NOT NULL,
                                    base INTEGER NOT NULL,
                                    strand TEXT NOT NULL,
                                    coverage INTEGER NOT NULL,
                                    freqC REAL NOT NULL,
                                    freqT REAL NOT NULL,
                                    bioinfoexperiment_id INTEGER NOT NULL,
                                    FOREIGN KEY(bioinfoexperiment_id) REFERENCES MetaBioinfoexperiment(bioinfoexperiment_id)
                                )
                            ''',
                            '''
                                CREATE TABLE MethylkitMetadata(
                                    id INTEGER PRIMARY KEY AUTOINCREMENT,
                                    description TEXT NULL,
                                    study_design TEXT NOT NULL
                                )
                            ''',
                            '''
                                CREATE TABLE MethylkitMetadataBioinfoexperiment(
                                    methylkit_metadata_id INT,
                                    methylkit_bioinfoexperiment_id INT,
                                    FOREIGN KEY(methylkit_metadata_id) REFERENCES MethylkitMetadata(id),
                                    FOREIGN KEY(methylkit_bioinfoexperiment_id) REFERENCES MetaBioinfoexperiment(bioinfoexperiment_id)
                                )
                            ''',
                            '''
                                CREATE TABLE MethylkitDMRAnnotationType(
                                    id INTEGER PRIMARY KEY AUTOINCREMENT,
                                    description TEXT NOT NULL
                                )
                            ''',
                            '''
                                CREATE TABLE MethylkitDMR(
                                    id INTEGER PRIMARY KEY AUTOINCREMENT,
                                    chr TEXT NOT NULL,
                                    start INT NOT NULL,
                                    end INT NOT NULL,
                                    width INT NOT NULL,
                                    strand TEXT NOT NULL,
                                    pvalue REAL NOT NULL,
                                    qvalue REAL NOT NULL,
                                    diff_meth_perc INT NOT NULL,
                                    methylkit_metadata_id INT,
                                    FOREIGN KEY(methylkit_metadata_id) REFERENCES MethylkitMetadata(id)
                                )
                            ''',
                            '''
                                CREATE TABLE MethylkitDMRAnnotation(
                                    id INTEGER PRIMARY KEY AUTOINCREMENT,
                                    chr TEXT NOT NULL,
                                    start INT NOT NULL,
                                    end INT NOT NULL,
                                    width INT NOT NULL,
                                    strand TEXT NOT NULL,
                                    annotatr_annot_id TEXT NOT NULL,
                                    ucsc_gene_id TEXT NULL,
                                    entrez_gene_id INT NULL,
                                    ensembl_gene_id INT NULL,
                                    gene_symbol TEXT NULL,
                                    annotatr_type_db TEXT NOT NULL,
                                    methylkit_dmr_id INT,
                                    annotation_type_id INT,
                                    FOREIGN KEY(methylkit_dmr_id) REFERENCES MethylkitDMR(id)
                                    FOREIGN KEY(annotation_type_id) REFERENCES MethylkitDMRAnnotationType(id)
                                )
                            ''']
                            }
        self.__basePath = os.getcwd()
        self.__pathToDB = os.path.join(self.__basePath, dbName)
        # __basename = dbName + '.dict.pickle'
        # self.__pickleFile = os.path.join(self.__basePath, __basename)
        # self.__pickleFile = self.__basePath + '/' + __basename + '.dict.pickle'
        # if Path(self.__pickleFile).exists():
        #     with open(self.__pickleFile, 'rb') as pickleFile:
        #        self.__dict = pickle.load(pickleFile)
        # else:
        #     self.__dict = {}

    def getFile(self, action):
        return self.__csvFilesDict[action]

    def getTableName(self, action):
        return self.__tableNamesDict[action]

    def getSchema(self, action):
        return self.__schemaDict[action]

    def getColName(self, action):
        return self.__colNamesDict[action]

    # def __updataPickle(self):
    #     with open(self.__pickleFile, "wb") as outfile:
    #         pickle.dump(self.__dict, outfile)

    def createConnection(self):
        """Create connection to the database with the give path.

        If the database cannot be found with the given path, a new
        database is going to be created under the given path.

        Parameters
        ----------
        pathToDB: str
            Full path to the database

        Return
        ------
        An instance of sqlite3 object
        """
        conn = None
        if Path(self.__pathToDB).exists():
            conn = sqlite3.connect(self.__pathToDB)
        else:
            print("There's no such a database. \n")
            print("A new database is going to be created as the given name. \n")
            Path(self.__pathToDB).touch()
            conn = sqlite3.connect(self.__pathToDB)
        conn.execute("PRAGMA foreign_keys = On")
        conn.commit()
        return conn

    def createTable(self, conn, tableName, schema):
        """Create table/tables in the given database with given schema.

        The schema must be wrapped by '''''', and the data type of
        each entity should be claimed in the schema. For instance, I am
        going to create a table "person" which has attributes person_id,
        firstName, lastName. Then, the schema passed into this function
        should be written as the following:
        '''
        CREATE TABLE person(
            person_id TEXT PRIMARY KEY,
            firstName TEXT,
            lastName TEXT
        )
        '''
        Since the method executescript(sql_script) is going to be used,
        the schema could contain multiple tables.

        Parameter
        ---------
        conn: An instance of the sqlite3 Connection object
            The estabilished connection to the target database
        tableName: str
            The name of the new created table.
        schema: str
            The sqlite3 scripts for creating one table or tables

        Raises
        ------
        OperationalError: if a table with the same name existed in the
        database
        """
        c = conn.cursor()
        #try:
        if isinstance(schema, list):
            for index in range(len(schema)):
                c.executescript(schema[index])
        #         self.__dict[tableName[index]] = set()
        else:
            c.executescript(schema)
        #     self.__dict[tableName] = set()
        #except sqlite3.OperationalError:
        #    print("Table {} exists".format(tableName))

    def csvToSql(self, conn, tableName, colName, fileName):
        """This function converses a csv file into an sqlite table
        by calling the function .to_sql provided by pandas

        Parameter
        ---------
        conn:An instance of the sqlite3 Connection object
            The estabilished connection to the target database
        tableName: str
            The name of the table to which the csv file is going
            to convert.
        colName: str
            The names of the colnums in the table. The order of
            the names as well as the spelling of names must be
            exact the same as the schema.
        fileName: str
            The name of a csv file.
        """

        __basePath = os.getcwd()
        csv_file_path = os.path.join(__basePath, fileName)
        if os.path.exists(csv_file_path) == True:
            # tmpSet = self.__dict[tableName]
            # if csv_file_path not in tmpSet:
            with open(csv_file_path, 'rb') as rawdata:
                result = chardet.detect(rawdata.read(10000))
            encoding = result['encoding']
            length = os.path.getsize(csv_file_path)
            print(length)
            print(encoding)
            if length > 0:
                csv_file = pd.read_csv(csv_file_path, encoding = encoding, header = None)
                csv_file.columns = colName
                csv_file.to_sql(tableName, conn, if_exists = 'append', index = False)
                # tmpSet.add(csv_file_path)
                # self.__dict[tableName] = tmpSet
                # self.__updataPickle()
            else:
                print("This file has 0 lines")
            # else:
            #     print("{} has been inserted into the database".format(csv_file_path))
        else:
            print("{} does not exist".format(csv_file_path))

    def gtfToSql(self, conn, tableName, colName, fileName):
        __basePath = os.getcwd()
        gtf_file_path = os.path.join(__basePath, fileName)
        if os.path.exists(gtf_file_path) == True:
            # tmpSet = self.__dict[tableName]
            # if gtf_file_path not in tmpSet:
            df = read_gtf(gtf_file_path)
            df.reset_index(inplace = True)
            df['index'] = df['index'] + 1
            df = df[['index', 'seqname', 'start', 'end', 'strand', 'source', 'feature', 'gene_id', 'transcript_id',
                  'gene_name', 'ref_gene_id']]
            df.columns = colName
            df.to_sql(tableName, conn, if_exists = 'append', index = False)
            #  tmpSet.add(gtf_file_path)
            # self.__dict[tableName] = tmpSet
            self.__updataPickle()
            # else:
            #     print("{} has been inserted into the database".format(gtf_file_path))
        else:
            print("{} does not exist".format(gtf_file_path))

    def studyDesignToSql(self, conn, tableName, colName, fileName):
        basePath = os.getcwd()
        txt_fiel_path = os.path.join(basePath, fileName)
        if os.path.exists(txt_fiel_path):
            # tmpSet = self.__dict[tableName]
            # if txt_fiel_path not in tmpSet:
            df = pd.read_csv(txt_fiel_path, delimiter = '\t')
            df.columns = colName
            df.to_sql(tableName, conn, if_exists = 'append', index = False)
            # tmpSet.add(txt_fiel_path)
            # self.__dict[tableName] = tmpSet
            # self.__updataPickle()
            # else:
            #     print("{} has been inserted into the database".format(txt_fiel_path))
        else:
            print("{} does not exist".format(txt_fiel_path))

    def methylationToSql(self, conn, tableName, colName, fileName):
        basePath = os.getcwd()
        txt_fiel_path = os.path.join(basePath, fileName)
        if os.path.exists(txt_fiel_path):
            # tmpSet = self.__dict[tableName]
            # if txt_fiel_path not in tmpSet:
            df = pd.read_csv(txt_fiel_path, delimiter = '\t', header = None)
            df.columns = colName
            df.to_sql(tableName, conn, if_exists = 'append', index = False)
            # tmpSet.add(txt_fiel_path)
            # self.__dict[tableName] = tmpSet
            # self.__updataPickle()
            # else:
            #     print("{} has been inserted into the database".format(txt_fiel_path))
        else:
            print("{} does not exist".format(txt_fiel_path))



    def dropTable(self, conn, tableName):
        """Drop a table in the given database.

        Parameter
        ---------
        conn: An instance of the sqlite3 Connection object
            The established connection to the target database
        tableName: str
            The name of the table is going to be dropped.
        script: str
            The script drops the given table. The script must
            be a string wrapped in ''''''

        Return:
            An connection to the updated database
        """
        script = '''DROP TABLE {}'''.format(tableName)
        c = conn.cursor()
        c.executescript(script)
        # self.__dict.pop(tableName)
        # self.__updataPickle()

    '''
    def importNovelTData(self, conn, dirName):
        listPath = os.path.join(self.__basePath, dirName)
        fileList = os.listdir(listPath)
        fileList.sort()
        for index in range(len(fileList)):
            ids = fileList[index].split("_")
            filePath = os.path.join(self.__basePath, dirName, fileList[index], "t_data.ctab")
            print(filePath)
            tableName = "{}".format(ids[2])
            print(tableName)
            if tableName not in self.__dict:
                tmp = pd.read_table(filePath)
                tmp["Experiment_ID"] = int(ids[2])
                tmp["Sample_Name"] = fileList[index]
                print(tableName)
                print(fileList[index])
                tmp.to_sql(tableName, conn)
                tmpSet = set()
                tmpSet.add(filePath)
                self.__dict[tableName] = tmpSet
                self.__updataPickle()
            else:
                print("{} has been inserted into the database".format(filePath))
    '''
    def dbExistsTable(self, conn, tableName):
        query = '''SELECT count(name) FROM sqlite_master WHERE type = "table" AND name = "{tableName}"'''.format(tableName = tableName)
        c = conn.cursor()
        c.execute(query)
        if c.fetchone()[0] == 1:
            return True
        else:
            return False

def main():
    conn = None
    operato = None
    script = sys.argv[0]
    action = sys.argv[1]
    db = sys.argv[2]
    if len(sys.argv) >= 4:
        file = sys.argv[3]
    normalOperation = {"--rna_genefusions", "--rna_expmut", "--rna_expmutann", "--cnv_segmented",
                      "--dna_mutmetadata", "--dna_mutations", "--rna_geneabundances", "--rna_genecounts", "--gtf",
                      "--hg19Bands", "--hg19GeneIDs", "--hg19BandsToGeneIDs", "--metaBioexp", "--studyDesign", "--methyldackel"}
    assert action in ["--rna_genefusions", "--rna_expmut", "--rna_expmutann", "--cnv_segmented", "--all",
                      "--dna_mutmetadata", "--hg19Bands", "--hg19GeneIDs", "--hg19BandsToGeneIDs",
                      "--dna_mutations", "--rna_geneabundances", "--rna_genecounts", "--gtf", "--dropTable", "--importTdata","--metaBioexp", "--studyDesign", "--methyldackel"], \
                      'Action is not proper: ' + action
    operator = DBOperator(db)
    conn = operator.createConnection()
    if action in normalOperation:

        if conn is not None:
            tableName = operator.getTableName(action)
            schema = operator.getSchema(action)
            colName = operator.getColName(action)
            fileName = operator.getFile(action)
            if operator.dbExistsTable(conn, tableName) == False and os.path.exists(os.path.join(os.getcwd(), fileName)) == True:
                operator.createTable(conn, tableName, schema)

            if action == "--gtf":
                operator.gtfToSql(conn, tableName, colName, fileName)
            elif action == "--studyDesign":
                operator.studyDesignToSql(conn, tableName, colName, fileName)
            elif action == "--methyldackel":
                operator.csvToSql(conn, tableName[0], colName[0], fileName)
            else:
                operator.csvToSql(conn, tableName, colName, fileName)
        else:
            print("Please create a connection first")
    elif action == "--dropTable":
        if conn is not None:
            operator.dropTable(conn, file)
        else:
            print("Please create a connection first")
    elif action == "--importTdata":
        if conn is not None:
            operator.importNovelTData(conn, file)
        else:
            print("Please create a connection first")
    elif action == "--all":
        actions = ["--metaBioexp","--rna_genefusions", "--rna_expmut", "--rna_expmutann", "--cnv_segmented", "--dna_mutmetadata", "--dna_mutations", "--rna_geneabundances", "--rna_genecounts", "--gtf", "--hg19Bands", "--hg19GeneIDs", "--hg19BandsToGeneIDs",  "--studyDesign", "--methyldackel"]
        for index in range(len(actions)):
            act = actions[index]
            print(act)
            tableName = operator.getTableName(act)
            schema = operator.getSchema(act)
            colName = operator.getColName(act)
            fileName = operator.getFile(act)
            if operator.dbExistsTable(conn, tableName) == False and os.path.exists(os.path.join(os.getcwd(), fileName)) == True:

                operator.createTable(conn, tableName, schema)
                if act == "--gtf":
                    operator.gtfToSql(conn, tableName, colName, fileName)
                elif act == "--studyDesign":
                    operator.studyDesignToSql(conn, tableName, colName, fileName)
                elif act == "--methyldackel":
                    operator.csvToSql(conn, tableName[0], colName[0], fileName)
                else:
                    operator.csvToSql(conn, tableName, colName, fileName)
    conn.close()

if __name__ == '__main__':
    main()
