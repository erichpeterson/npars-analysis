# V1.1
# This version is for importing the mock data of GMU20
import pandas as pd
import click

@click.command()
@click.option('--input', help = 'The path to the file which is going to be converted')
@click.option('--output', help = 'The path to the output file')
def transform(input, output):
    df = pd.read_csv(input, delimiter = ' ')
    header = df.columns.values
    id_col = header[len(header) - 1]
    sample_col = header[0 : len(header) - 1]
    frame_list = []
    for index in range(len(header) - 1):
        sample_name = sample_col[index]
        sample_id = sample_name # for GMU data, sample_id is a part of the sample name
                                # but for karbassi data, sample_id is the sample name
        if (sample_name.find('20') > 0):
            sample_id = experimentID_extractor(sample_name)
            print(sample_id)
        tmp_data = df[[id_col, sample_name]] # slice the data.
        tmp_data.columns = [id_col, 'scaled_tpm'] # rename the columns
        bioinfoexp_id = [sample_id] * len(df)
        tmp_data.insert(loc = 0, column = 'bioinfoexperiment_id', value = bioinfoexp_id)
        frame_list.append(tmp_data)
    transformed_data = pd.concat(frame_list)
    transformed_data.to_csv(output, index = False, header = False)

def experimentID_extractor(gmu_expID):
    return gmu_expID.split('_')[1]

if __name__ == '__main__':
    transform()
